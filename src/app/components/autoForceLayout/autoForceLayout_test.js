'use strict';

describe('autoForceLayout module', function () {

    var $compile, $rootScope, element;

    beforeEach(module('autoForceLayout'));
    function _provide(){
        module(function($provide) {
            $provide.service('$mdDialog', function() {
                this.show = function(){
                    return {
                        then: function(){

                        }
                    }
                }
            });
        });
    }

    function _setInDom() {
        inject(function (_$compile_, _$rootScope_) {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            $rootScope.aflOptions = {};
            element = $compile(angular.element('<div auto-force-layout options="aflOptions"></div>'))($rootScope);
        });
    }

    beforeEach(function(){
        _provide();
        _setInDom();
    });

    afterEach(function () {
        element.remove();
    });

    function getInstance(element) {
        var isoScope = element.scope().$$childHead;
        return isoScope.autoForceLayoutCtrl.options.autoForceLayoutInstance;
    }

    describe('autoForceLayout directive', function () {
        it('should create an echo factory instance', function () {
            expect(getInstance(element)).toBeDefined();
        });
    });
});